package enums

type ProcessType int

const (
	/**
	 * 业务流程
	 */
	business ProcessType = 0
	/**
	 * 子流程
	 */
	child ProcessType = 1
	/**
	 * 主流程
	 */
	main ProcessType = 2
)
