package enums

type NodeSetType int

const (
	/**
	 * 指定成员
	 */
	specifyMembers NodeSetType = 1
	/**
	 * 主管
	 */
	supervisor NodeSetType = 2
	/**
	 * 角色
	 */
	roleNodeSetType NodeSetType = 3
	/**
	 * 发起人自选
	 */
	initiatorSelected NodeSetType = 4
	/**
	 * 发起人自己
	 */
	initiatorThemselvesNodeSetType NodeSetType = 5
	/**
	 * 连续多级主管
	 */
	multiLevelSupervisors NodeSetType = 6
	/**
	 * 部门
	 */
	departmentNodeSetType NodeSetType = 7
)
