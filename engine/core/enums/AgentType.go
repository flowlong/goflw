package enums

/**
 * 代理人类型
 *
 * <p>
 * 尊重知识产权，不允许非法使用，后果自负
 * </p>
 *
 * @author hubin
 * @since 1.0
 */
type AgentType int

const (
	/**
	 * 代理人
	 */
	agentAgentType AgentType = 0
	/**
	 * 被代理人
	 */
	principal AgentType = 1
	/**
	 * 认领角色
	 */
	claimRole AgentType = 2
	/**
	 * 认领部门
	 */
	claimDepartment AgentType = 3
)
