package enums

type PerformType int

const (
	/**
	 * 发起
	 */
	startPerformType PerformType = 0
	/**
	 * 按顺序依次审批
	 */
	sort PerformType = 1
	/**
	 * 会签 (可同时审批，每个人必须审批通过)
	 */
	countersign PerformType = 2
	/**
	 * 或签 (有一人审批通过即可)
	 */
	orSign PerformType = 3
	/**
	 * 票签 (总权重大于节点 passWeight 属性)
	 */
	voteSign PerformType = 4
	/**
	 * 定时器
	 */
	timerPerformType PerformType = 5
	/**
	 * 触发器
	 */
	triggerPerformType PerformType = 6
	/**
	 * 抄送
	 */
	copyPerformType PerformType = 7
)
