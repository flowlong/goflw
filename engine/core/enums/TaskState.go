package enums

type TaskState int

// 流程状态
const (
	/**
	 * 活动
	 */
	activeTaskState TaskState = 0
	/**
	 * 跳转
	 */
	jumpTaskState TaskState = 1
	/**
	 * 完成
	 */
	completeTaskState TaskState = 2
	/**
	 * 拒绝
	 */
	rejectTaskState TaskState = 3
	/**
	 * 撤销审批
	 */
	revokeTaskState TaskState = 4
	/**
	 * 超时
	 */
	timeoutTaskState TaskState = 5
	/**
	 * 终止
	 */
	terminateTaskState TaskState = 6
	/**
	 * 驳回终止
	 */
	rejectEnd TaskState = 7
	/**
	 * 自动完成
	 */
	autoCompleteTaskState TaskState = 8
	/**
	 * 自动驳回
	 */
	autoRejectTaskState TaskState = 9
	/**
	 * 自动跳转
	 */
	autoJumpTaskState TaskState = 10
)
