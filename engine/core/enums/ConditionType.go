package enums

type ConditionType int

const (
	custom ConditionType = 0

	form ConditionType = 1
)
