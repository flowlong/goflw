package enums

/**
 * 审批人与提交人为同一人时
 *
 * <p>
 * 尊重知识产权，不允许非法使用，后果自负
 * </p>
 *
 * @author life
 * @since 1.0
 */
type NodeApproveSelf int

const (
	/**
	 * 由发起人对自己审批
	 */
	initiatorThemselves NodeApproveSelf = 0
	/**
	 * 自动跳过
	 */
	AutoSkip NodeApproveSelf = 1
	/**
	 * 转交给直接上级审批
	 */
	TransferDirectSuperior NodeApproveSelf = 2
	/**
	 * 转交给部门负责人审批
	 */
	TransferDepartmentHead NodeApproveSelf = 3
)
