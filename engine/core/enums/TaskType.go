package enums

type TaskType int

const (
	/**
	 * 结束节点
	 */
	end TaskType = -1
	/**
	 * 主办
	 */
	major TaskType = 0
	/**
	 * 审批
	 */
	approval TaskType = 1
	/**
	 * 抄送
	 */
	cc TaskType = 2
	/**
	 * 条件审批
	 */
	conditionNode TaskType = 3
	/**
	 * 条件分支
	 */
	conditionBranch TaskType = 4
	/**
	 * 调用外部流程任务【办理子流程】
	 */
	callProcess TaskType = 5
	/**
	 * 定时器任务
	 */
	timer TaskType = 6
	/**
	 * 触发器任务
	 */
	trigger TaskType = 7
	/**
	 * 并行分支
	 */
	parallelBranch TaskType = 8
	/**
	 * 包容分支
	 */
	inclusiveBranch TaskType = 9
	/**
	 * 转办、代理人办理完任务直接进入下一个节点
	 */
	transfer TaskType = 10
	/**
	 * 委派、代理人办理完任务该任务重新归还给原处理人
	 */
	delegate TaskType = 11
	/**
	 * 委派归还任务
	 */
	delegateReturn TaskType = 12
	/**
	 * 代理人任务
	 */
	agent TaskType = 13
	/**
	 * 代理人归还的任务
	 */
	agentReturn TaskType = 14
	/**
	 * 代理人协办完成的任务
	 */
	agentAssist TaskType = 15
	/**
	 * 被代理人自己完成任务
	 */
	agentOwn TaskType = 16
)
