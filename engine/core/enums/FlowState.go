package enums

type FlowState int

const (
	/**
	 * 启用
	 */
	active FlowState = 1
	/**
	 * 未启用
	 */
	inactive FlowState = 0
	/**
	 * 历史版本
	 */
	history FlowState = 2
)
