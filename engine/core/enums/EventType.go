package enums

type EventType int

const (
	/**
	 * 发起
	 */
	start EventType = 0
	/**
	 * 创建
	 */
	create EventType = 1
	/**
	 * 再创建，仅用于流程回退
	 */
	recreate EventType = 2
	/**
	 * 抄送
	 */
	ccEventType EventType = 3
	/**
	 * 分配
	 */
	assignment EventType = 4
	/**
	 * 委派任务解决
	 */
	delegateResolve EventType = 5
	/**
	 * 任务加签
	 */
	addTaskActor EventType = 6
	/**
	 * 任务减签
	 */
	removeTaskActor EventType = 7
	/**
	 * 驳回至上一步处理
	 */
	reject EventType = 8
	/**
	 * 角色认领
	 */
	claimRoleEventType EventType = 9
	/**
	 * 部门认领
	 */
	claimDepartmentEventType EventType = 10
	/**
	 * 拿回未执行任务
	 */
	reclaim EventType = 11
	/**
	 * 撤回指定任务
	 */
	withdraw EventType = 12
	/**
	 * 唤醒历史任务
	 */
	resume EventType = 13
	/**
	 * 完成
	 */
	complete EventType = 14
	/**
	 * 撤销
	 */
	revoke EventType = 15
	/**
	 * 终止
	 */
	terminate EventType = 16
	/**
	 * 更新
	 */
	update EventType = 17
	/**
	 * 删除
	 */
	deleteEventType EventType = 18
	/**
	 * 调用外部流程任务【办理子流程】
	 */
	callProcessEventType EventType = 19
	/**
	 * 超时
	 */
	timeout EventType = 20
	/**
	 * 跳转
	 */
	jump EventType = 21
	/**
	 * 自动跳转
	 */
	autoJump EventType = 22
	/**
	 * 自动审批完成
	 */
	autoComplete EventType = 23
	/**
	 * 自动审批拒绝
	 */
	autoReject EventType = 24
	/**
	 * 触发器任务
	 */
	triggerEventType EventType = 25
	/**
	 * 结束
	 */
	endEventType EventType = 26
)
