package enums

type ActorType int

const (
	/**
	 * 用户
	 */
	user ActorType = 0
	/**
	 * 角色
	 */
	role ActorType = 1
	/**
	 * 部门
	 */
	department ActorType = 2
)
