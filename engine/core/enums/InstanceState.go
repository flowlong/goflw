package enums

type InstanceState int

/**
 * 流程状态
 *
 * <p>
 * 尊重知识产权，不允许非法使用，后果自负
 * </p>
 *
 * @author life
 * @since 1.0
 */
const (
	/**
	 * 审批中
	 */
	activeInstanceState InstanceState = 0
	/**
	 * 审批通过
	 */
	completeInstanceState InstanceState = 1
	/**
	 * 审批拒绝【 驳回结束流程 】
	 */
	rejectInstanceState InstanceState = 2
	/**
	 * 撤销审批
	 */
	revokeInstanceState InstanceState = 3
	/**
	 * 超时结束
	 */
	timeoutInstanceState InstanceState = 4
	/**
	 * 强制终止
	 */
	terminateInstanceState InstanceState = 5
)
