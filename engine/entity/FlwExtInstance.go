package entity

type FlwExtInstance struct {
	/**
	 * 主键ID
	 */
	id int64
	/**
	 * 租户ID
	 */
	tenantId string
	/**
	 * 流程定义ID
	 */
	processId string
	/**
	 * 流程定义类型（冗余业务直接可用）
	 */
	processType string
	/**
	 * 流程模型定义JSON内容
	 * <p>
	 * 在发起的时候拷贝自流程定义模型内容，用于记录当前实例节点的动态改变。
	 * </p>
	 */
	modelContent string
}
