package entity

/**
 * 任务参与者实体类
 */
type FlwTaskActor struct {
	/**
	 * 主键ID
	 */
	id int64
	/**
	 * 租户ID
	 */
	tenantId string
	/**
	 * 流程实例ID
	 */
	instanceId int64
	/**
	 * 关联的任务ID
	 */
	taskId int64
	/**
	 * 关联的参与者ID（参与者可以为用户、部门、角色）
	 */
	actorId string
	/**
	 * 关联的参与者名称
	 */
	actorName string
	/**
	 * 参与者类型 0，用户 1，角色 2，部门
	 */
	actorType int
	/**
	 * 权重
	 * <p>
	 * 票签任务时，该值为不同处理人员的分量比例
	 * </p>
	 */
	weight int
	/**
	 * 代理人ID
	 */
	agentId string
	/**
	 * 代理人类型 0，代理 1，被代理 2，认领角色 3，认领部门
	 */
	agentType int
	/**
	 * 扩展json
	 */
	extend string
}
