package entity

type FlowEntity struct {
	/**
	 * 主键ID
	 */
	id int
	/**
	 * 租户ID
	 */
	tenantId string
	/**
	 * 创建人ID
	 */
	createId string
	/**
	 * 创建人名称
	 */
	createBy string
	/**
	 * 创建时间
	 */
	createTime string
}
