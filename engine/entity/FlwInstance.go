package entity

import "time"

type FlwInstance struct {
	FlowEntity
	/**
	 * 流程定义ID
	 */
	processId int64
	/**
	 * 父流程实例ID
	 */
	parentInstanceId int64
	/**
	 * 流程实例优先级
	 */
	priority int
	/**
	 * 流程实例编号
	 */
	instanceNo string
	/**
	 * 业务KEY（用于关联业务逻辑实现预留）
	 *
	 * <p>
	 * 子流程情况，该字段用于存放父流程所在节点KEY
	 * </p>
	 */
	businessKey string
	/**
	 * 变量json
	 */
	variable string
	/**
	 * 当前所在节点名称
	 */
	currentNodeName string
	/**
	 * 当前所在节点key
	 */
	currentNodeKey string
	/**
	 * 流程实例期望完成时间
	 */
	expireTime time.Time
	/**
	 * 流程实例上一次更新人
	 */
	lastUpdateBy string

	lastUpdateTime time.Time
}
