package entity

import (
	"time"
)

type FlwTask struct {
	FlowEntity
	/**
	 * 流程实例ID
	 */
	instanceId int64
	/**
	 * 父任务ID
	 */
	parentTaskId int64
	/**
	 * 任务名称
	 */
	taskName string
	/**
	 * 任务 key 唯一标识
	 */
	taskKey string
	/**
	 * 任务类型
	 */
	taskType int
	/**
	 * 参与方式 {@link PerformType}
	 */
	performType int
	/**
	 * 任务关联的表单url
	 */
	actionUrl int
	/**
	 * 变量json
	 */
	variable string
	/**
	 * 委托人ID
	 */
	assignorId string
	/**
	 * 委托人
	 */
	assignor string
	/**
	 * 期望任务完成时间
	 */
	expireTime time.Time
	/**
	 * 提醒次数
	 */
	remindRepeat time.Time
	/**
	 * 已阅 0，否 1，是
	 */
	viewed int
}
