package entity

import "time"

type FlwHisInstance struct {
	FlwInstance
	/**
	 * 状态 0，活动 1，结束
	 */
	instanceState int
	/**
	 * 结束时间
	 */
	endTime time.Time
	/**
	 * 处理耗时
	 */
	duration int64
}
