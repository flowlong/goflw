package entity

import "time"

type FlwHisTask struct {
	FlwTask
	/**
	 * 调用外部流程定义ID
	 */
	callProcessId int64
	/**
	 * 调用外部流程实例ID
	 */
	callInstanceId int64
	/**
	 * 完成时间
	 */
	finishTime time.Time
	/**
	 * 任务状态 0，活动 1，跳转 2，完成 3，拒绝 4，撤销审批  5，超时 6，终止 7，驳回终止
	 */
	taskState int
	/**
	 * 处理耗时
	 */
	duration int64
}
