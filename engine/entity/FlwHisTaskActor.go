package entity

/**
 * 历史任务参与者实体类
 *
 * <p>
 * 尊重知识产权，不允许非法使用，后果自负
 * </p>
 *
 * @author life
 * @since 1.0
 */
type FlwHisTaskActor struct {
	FlwTaskActor
}
