package entity

type FlwProcess struct {
	FlowEntity
	/**
	 * 流程定义 key 唯一标识
	 */
	processKey string
	/**
	 * 流程定义名称
	 */
	processName string
	/**
	 * 流程图标地址
	 */
	processIcon string
	/**
	 * 流程定义类型（预留字段）
	 */
	processType string
	/**
	 * 流程版本
	 */
	processVersion string
	/**
	 * 当前流程的实例url（一般为流程第一步的url）
	 * 该字段可以直接打开流程申请的表单
	 */
	instanceUrl string
	/**
	 * 备注说明
	 */
	remark string
	/**
	 * 使用范围 0，全员 1，指定人员（业务关联） 2，均不可提交
	 */
	useScope string
	/**
	 * 流程状态 0，不可用 1，可用 2，历史版本
	 */
	processState string
	/**
	 * 流程模型定义JSON内容
	 */
	modelContent string
	/**
	 * 排序
	 */
	sort string
}
